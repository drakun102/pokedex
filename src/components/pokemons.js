const Pokemons = ({ pokemons }) => {
    return (
        <div>
        <center><h1>Pokedex</h1></center>
       
        <div className="row">
        <div className="col-1"></div>
         <div className="col-10">
               
          <div className="card">
            <div className="card-header">
            <center><h1>{pokemons.name} </h1>N.-{pokemons.id}</center>
            </div>
            <div className="card-body">
            <center><h1 className="card-title"></h1></center>
              <div className="row">
                  <div className="col-2">
                    <img width="150px" src={pokemons.sprites.front_default} alt=""></img>
                  </div>
                  <div className="col-4">
                      <div className="row">
                          <div className="col-6">
                            <center><label><h3>Tipo</h3></label></center>
                            <ul className="pokemon-types">
                            {pokemons.types.map(item => (
                                <Ptypes key={item.type.name} item={item}/>
                            ))}
                            </ul>
                          </div>
                          <div className="col-6">
                          <center><label><h3>Habilidades</h3></label></center>
                          <ul className="pokemon-abilities">
                            {pokemons.abilities.map(item => (
                                <Abilities key={item.ability.name} item={item}/>
                            ))}
                            </ul>
                          </div>

                      </div>
                    
                  </div>
                  <div className="col-3">
                  <center><label><h3>Puntos Base</h3></label></center>
                    <ul className="pokemon-stats">
                    {pokemons.stats.map(itemjj => (
                        <Stats key={itemjj.stat.name} itemjj={itemjj}/>
                    ))}
                    </ul>
                  </div>
                  <div className="col-3" >
                  <center><label><h3>Movimientos</h3></label></center>
                    <ul className="pokemon-moves">
                    {pokemons.moves.filter((item, idx) => idx < 5).map(itemj => (
                        <Moves key={itemj.move.name} itemj={itemj}/>
                    ))}
                    </ul>
                  </div>
                  

              </div>
              
             </div> 
              
              
              
            </div>
            </div>
            <div className="col-1"></div>              
         </div>

        
       
      </div>
    
    )
  };

  function Ptypes({ item }){
    return (
      <li className="pokemon-types" >
        <button className="btn btn-secondary"><b>{item.type.name} </b></button>
        
      </li>
      
    )
  }

  function Moves({ itemj }){
    return (
      <li className="pokemon-moves">
        <span className="m-name"><b>{itemj.move.name} </b></span>
        
      </li>
    )
  }

  function Stats({ itemjj }){
    return (
      <li className="pokemon-moves">
        <span className="m-name"><b>{itemjj.stat.name} </b></span>
        <span className="m-base"><b>{itemjj.base_stat} </b></span>
        
      </li>
    )
  }

  function Abilities({ item }){
    return (
      <li className="pokemon-abilities">
        <span className="m-name"><b>{item.ability.name} </b></span>
        
        
      </li>
    )
  }
  
  export default Pokemons