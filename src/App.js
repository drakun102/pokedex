
import React from 'react';
import './App.css';
import Pokemons from './components/pokemons';
import PokemonDetail from './components/pokemonsDetails';




class App extends React.Component{

  constructor(props) {
    super(props);
    this.state = {
                pokemons: [],
                search: false,
                value:''};

    // bind function to component
    this.getdatafrom = this.getdatafrom.bind(this);
  }

  

  getdatafrom(e) {
    let poke= e.target.value;
    console.log("dato"+poke);
    if(poke){
      fetch('https://pokeapi.co/api/v2/pokemon/'+poke)
    .then(res => res.json())
    .then((data) => {
      this.setState({ pokemons: data ,search: true  })
     
    })
    .catch(this.setState({ search: false  }) )
    }
    

    
  }

  render(){
    const isSearch = this.state.search;
    let datarender;
    
    if (isSearch){
        datarender = 
        <div className="content">
            <center><input type="text" name="boxs"   placeholder="Buscar Pokémon" onChange = {this.getdatafrom}/></center>
            <Pokemons pokemons={this.state.pokemons} />
            <PokemonDetail pokemonsDetails={this.state.pokemons} />
        </div>;
        

      
    }
    else{
      datarender = <div className="content">
      <center><input type="text" name="boxs"   placeholder="Buscar Pokemon" onChange = {this.getdatafrom}/></center>
      <center><p>No se encontro pokémon</p></center>
     
    </div>;
     
    }

    return(
      <div>
           {datarender}
      </div>
    
     
        
        
      
      );
    

    
  }

}

export default App;
